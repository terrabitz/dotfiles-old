set -g mouse on

# Have default functionality be making xonsh be the default shell only in tmux
# This will prevent issues in Ansible provisioning
set-option -g default-shell /usr/bin/zsh
set-window-option -g mode-keys vi
set-option -s set-clipboard off
bind-key -T copy-mode-vi MouseDragEnd1Pane send-keys -X copy-pipe-and-cancel "xclip -se c -i"

##### Bindings #####
bind-key -n C-s setw synchronize-panes
bind-key k confirm kill-window
bind-key K confirm kill-server
bind-key C-t source-file ~/.tmux.conf

## Bind Navigation Keys
# Arrow Key Bindings
bind-key -n S-M-Left select-window -t -1
bind-key -n S-M-Right select-window -t +1
bind-key -n C-S-Left swap-window -t -1
bind-key -n C-S-Right swap-window -t +1
# VIM bindings
is_vim="ps -o state= -o comm= -t '#{pane_tty}' | grep -iqE '^[^TXZ ]+ +(\\S+\\/)?g?(view|n?vim?x?)(diff)?$'"
bind-key -n C-h if-shell "$is_vim" "send-keys C-h"  "select-pane -L"
bind-key -n C-j if-shell "$is_vim" "send-keys C-j"  "select-pane -D"
bind-key -n C-k if-shell "$is_vim" "send-keys C-k"  "select-pane -U"
bind-key -n C-l if-shell "$is_vim" "send-keys C-l"  "select-pane -R"
bind-key -n C-\ if-shell "$is_vim" "send-keys C-\\" "select-pane -l"
bind-key -T copy-mode-vi C-h select-pane -L
bind-key -T copy-mode-vi C-j select-pane -D
bind-key -T copy-mode-vi C-k select-pane -U
bind-key -T copy-mode-vi C-l select-pane -R
bind-key -T copy-mode-vi C-\ select-pane -l
bind-key -n M-h select-window -t -1
bind-key -n M-l select-window -t +1

# don't rename windows automatically
#set-option -g allow-rename off
#set-window-option -g automatic-rename off

# Fix to allow mousewheel/trackpad scrolling in tmux 2.1
bind-key -T root WheelUpPane if-shell -F -t = "#{alternate_on}" "send-keys -M" "select-pane -t =; copy-mode -e; send-keys -M"
bind-key -T root WheelDownPane if-shell -F -t = "#{alternate_on}" "send-keys -M" "select-pane -t =; send-keys -M"

# List of plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'tmux-plugins/tmux-copycat'
set -g @plugin 'tmux-plugins/tmux-resurrect'
set -g @plugin 'tmux-plugins/tmux-continuum'
set -g @plugin 'jimeh/tmux-themepack'
set -g @themepack 'powerline/double/cyan'

# Other examples:
# set -g @plugin 'github_username/plugin_name'
# set -g @plugin 'git@github.com/user/plugin'
# set -g @plugin 'git@bitbucket.com/user/plugin'

# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run -b '~/.tmux/plugins/tpm/tpm'
