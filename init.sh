#!/bin/bash

sudo apt update

cp -R -f ./files/. ~

if [ ! -d ~/.vim/autoload/plug.vim ]
then
    curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
fi

cd ~
vim +PlugInstall +qall

sudo apt install -y python3 python3-pip git vim tmux git-flow python3-dev zsh thefuck
git clone https://github.com/romkatv/powerlevel10k.git /home/terrabitz/.oh-my-zsh/custom/themes/powerlevel10k

sudo apt install -y aria2 youtube-dl

if [ ! -d ~/.config/kitty/kitty-themes ]
then
    git clone --depth 1 https://github.com/dexpota/kitty-themes  ~/.config/kitty/kitty-themes
    ln -s ~/.config/kitty/kitty-themes/themes/gruvbox_dark.conf ~/.config/kitty/theme.conf
fi

if [ ! -d ~/.tmux/plugins/tpm ]
then
    git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
fi
#echo $XONSHPATH | sudo tee -a /etc/shells > /dev/null
#sudo chsh -s $XONSHPATH $(whoami)

# Change the default Ubuntu terminal
#gsettings set org.gnome.desktop.default-applications.terminal exec '/home/terrabitz/.local/kitty.app/bin/kitty'

gsettings set org.gnome.desktop.interface monospace-font-name 'Hack Nerd Font 11' 

# Rebind the caps lock to ctrl
setxkbmap -option caps:ctrl_modifier
